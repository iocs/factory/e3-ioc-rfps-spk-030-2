require essioc
require rf_power_station
require iocmetadata

epicsEnvSet("SPK_SECTION", "030")
epicsEnvSet("INSTANCE", "2")
epicsEnvSet("HOSTNAME", "rfps030-2.tn.esss.lu.se")

iocshLoad("$(essioc_DIR)/common_config.iocsh", "ASG_SUBS='P=Spk-$(SPK_SECTION)RFC:RFS-RFPS-0$(INSTANCE)0, R=:'")

iocshLoad("$(rf_power_station_DIR)/rf_power_station.iocsh", "DEVICENAME=Spk-$(SPK_SECTION)RFC:RFS-RFPS-0$(INSTANCE)0, HOSTNAME=$(HOSTNAME), PORCH=Spk-$(SPK_SECTION)RFC:, RORCH=SC-FSM-001:")

pvlistFromInfo("ARCHIVE_THIS","$(IOCNAME):ArchiverList")

